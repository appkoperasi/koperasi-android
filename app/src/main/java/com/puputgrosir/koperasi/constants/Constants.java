package com.puputgrosir.koperasi.constants;

public class Constants {
    public static final String PROTOCOL = "https";
    public static final String HOST = "puputgrosir.org/koperasi";
    public static final String PHP_URL = PROTOCOL+"://"+HOST+"/php/";
}
