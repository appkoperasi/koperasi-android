package com.puputgrosir.koperasi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.puputgrosir.koperasi.constants.Constants;

public class LoginActivity extends BaseActivity {
    EditText phoneField, passwordField;
    TextView newUserView;
    GoogleSignInClient signInAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        phoneField = findViewById(R.id.phone);
        passwordField = findViewById(R.id.password);
        newUserView = findViewById(R.id.new_user);
        passwordField.setTransformationMethod(new PasswordTransformationMethod());
        newUserView.setText(Html.fromHtml(getResources().getString(R.string.new_user)+" "
                        +getResources().getString(R.string.signup)));
    }

    public void login(View view) {
        final String phone = phoneField.getText().toString().trim();
        final String password = passwordField.getText().toString().trim();
        if (phone.equals("")) {
            show(R.string.enter_phone);
            return;
        }
        if (password.equals("")) {
            show(R.string.enter_password);
            return;
        }
        post(new Listener() {

            @Override
            public void onResponse(String response) {
                if (response.equals("0")) {
                    
                } else if (response.equals("1")) {

                }
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, Constants.PHP_URL + "main/cek_user");
    }

    public void loginGoogle(View view) {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            String googleUid = account.getId();

        } else {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestId()
                    .requestEmail()
                    .build();
            signInAccount = GoogleSignIn.getClient(this, gso);
        }
    }

    public void loginFacebook(View view) {

    }

    public void loginTwitter(View view) {

    }
}
