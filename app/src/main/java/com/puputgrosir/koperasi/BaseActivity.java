package com.puputgrosir.koperasi;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.puputgrosir.koperasi.constants.Constants;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.util.concurrent.TimeUnit;

public class BaseActivity extends AppCompatActivity {

    public void log(String message) {
        Log.e(Build.MODEL, message);
    }

    public void show(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void show(int messageID) {
        Toast.makeText(this, messageID, Toast.LENGTH_SHORT).show();
    }

    public void run(Runnable runnable) {
        new Thread(runnable).start();
    }

    public void runLater(Runnable runnable) {
        new Handler(Looper.getMainLooper()).post(runnable);
    }

    public void post(final Listener listener, final String url, final String... params) {
        run(new Runnable() {

            @Override
            public void run() {
                try {
                    OkHttpClient client = getOkHttpClient();
                    MultipartBuilder builder = new MultipartBuilder()
                            .type(MultipartBuilder.FORM);
                    for (int i=0; i<params.length; i+=2) {
                        builder.addFormDataPart(params[i], params[i+1]);
                    }
                    Request request = new Request.Builder()
                            .post(builder.build())
                            .url(url)
                            .build();
                    final String response = client.newCall(request).execute().body().string();
                    runLater(new Runnable() {

                        @Override
                        public void run() {
                            if (listener != null) {
                                listener.onResponse(response);
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public OkHttpClient getOkHttpClient() {
        OkHttpClient client = new OkHttpClient();
        client.setReadTimeout(60, TimeUnit.SECONDS);
        client.setConnectTimeout(60, TimeUnit.SECONDS);
        client.setWriteTimeout(60, TimeUnit.SECONDS);
        return client;
    }

    public interface Listener {

        void onResponse(String response);
    }
}
